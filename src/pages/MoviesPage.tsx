import { Filters } from "components/Movie/Filters";
import { MovieFilters } from "types/domain/ui/Movie";
import { FC, useCallback, useMemo, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroller";
import {
  Box,
  Container,
  Drawer,
  LinearProgress,
  Paper,
  Typography,
} from "@mui/material";
import { useMovies } from "hooks/api/useMovies";
import { Card } from "components/Movie/Card";
import { appRoutes } from "routes/appRoutes";
import qs from "qs";
import { Details } from "components/Movie/Details";

export const MoviesPage: FC = () => {
  const [selectedMovieId, setSelectedMovieId] = useState<number | null>(null);
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  const resetSelectedMovieId = useCallback(() => {
    setSelectedMovieId(null);
  }, []);

  const handleSetSelectedMovieId = useCallback((movieId: number) => {
    setSelectedMovieId(movieId);
  }, []);

  const movieFilters = useMemo(() => {
    const primaryReleaseYear = searchParams.get("primaryReleaseYear");
    const voteAverage = searchParams.get("voteAverage");
    const genreIds = searchParams.get("genreIds");

    return {
      primaryReleaseYear: primaryReleaseYear
        ? Number(primaryReleaseYear)
        : undefined,
      voteAverage: voteAverage ? Number(voteAverage) : undefined,
      genreIds: genreIds?.split(",").map(Number),
    };
  }, [searchParams]);

  const handleSetMovieFilters = useCallback(
    (movieFilters: MovieFilters) => {
      navigate(
        `${appRoutes.movies()}?${qs.stringify(movieFilters, {
          arrayFormat: "comma",
        })}`
      );
    },
    [navigate]
  );

  const {
    movies,
    isFetching,
    isFetchingNextPage,
    hasNextPage,
    isFetched,
    fetchNextPage,
  } = useMovies(movieFilters);

  const handleLoadNextPage = useCallback(() => {
    fetchNextPage();
  }, [fetchNextPage]);

  return (
    <div className="App">
      <Box mb={8}>
        <Paper>
          <Box p={3}>
            <Filters
              disabled={isFetching}
              onSubmit={handleSetMovieFilters}
              initialMovieFilters={movieFilters}
            />
          </Box>
        </Paper>
      </Box>
      {isFetching && <LinearProgress />}
      <Container>
        <InfiniteScroll
          loadMore={handleLoadNextPage}
          hasMore={!isFetchingNextPage && hasNextPage}
          loader={<LinearProgress key={0} />}
        >
          {movies.map((movie) => (
            <Box mt={3} key={movie.id}>
              <Card movie={movie} onClick={handleSetSelectedMovieId} />
            </Box>
          ))}
        </InfiniteScroll>
        {!movies.length && isFetched && (
          <Typography>
            There are no movies with given search criteria
          </Typography>
        )}
      </Container>
      <Drawer
        anchor="right"
        open={!!selectedMovieId}
        onClose={resetSelectedMovieId}
      >
        {selectedMovieId && <Details movieId={selectedMovieId} />}
      </Drawer>
    </div>
  );
};
