import { Filters } from "components/Movie/Filters";
import { MovieFilters } from "types/domain/ui/Movie";
import { FC, useCallback } from "react";
import { useNavigate } from "react-router";
import { appRoutes } from "routes/appRoutes";
import qs from "qs";
import { Box } from "@mui/material";

export const MovieFiltersPage: FC = () => {
  const navigate = useNavigate();

  const handleSetMovieFilters = useCallback(
    (movieFilters: MovieFilters) => {
      navigate(
        `${appRoutes.movies()}?${qs.stringify(movieFilters, {
          arrayFormat: "comma",
        })}`
      );
    },
    [navigate]
  );

  return (
    <Box
      height="100vh"
      display="flex"
      flexDirection="column"
      justifyContent="center"
    >
      <Filters onSubmit={handleSetMovieFilters} />
    </Box>
  );
};
