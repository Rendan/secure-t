import axios from "axios";
import { camelizeKeys, decamelizeKeys } from "humps";

export const createHttpClient = <P extends Record<string, unknown>>(
  baseURL: string,
  defaultParams?: P
) => {
  const axiosInstance = axios.create({
    baseURL,
  });

  axiosInstance.interceptors.request.use((request) => {
    return {
      ...request,
      params: decamelizeKeys({ ...defaultParams, ...request.params }),
    };
  });
  axiosInstance.interceptors.response.use((response) => {
    return camelizeKeys(response.data);
  });

  return axiosInstance;
};
