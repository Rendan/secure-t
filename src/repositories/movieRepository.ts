import { GenresResponse, MovieResponse } from "types/responses/index";
import { movieApiRoutes } from "routes/movieApiRoutes";
import { createHttpClient } from "utils/http/client";
import { MovieFilters } from "types/domain/ui/Movie";
import { DiscoverMoviesResponse } from "types/responses";

const API_KEY = "117c16748a639561e9f8923f088211f1";

export const movieHttpClient = createHttpClient(
  `https://api.themoviedb.org/3`,
  { api_key: API_KEY }
);

export const movieRepository = {
  discoverMovies: (page: number, discoverMoviesLoadingParams: MovieFilters) => {
    const { voteAverage, genreIds, ...restParams } =
      discoverMoviesLoadingParams;
    return movieHttpClient.get<MovieFilters, DiscoverMoviesResponse>(
      movieApiRoutes.discoverMoviesPath(),
      {
        params: {
          page,
          "voteAverage.gte": voteAverage,
          withGenres: genreIds?.length ? genreIds.join(",") : undefined,
          ...restParams,
        },
      }
    );
  },
  genres() {
    return movieHttpClient.get<never, GenresResponse>(
      movieApiRoutes.genreMovieListPath()
    );
  },
  movie(movieId: number) {
    return movieHttpClient.get<never, MovieResponse>(
      movieApiRoutes.moviePath(movieId)
    );
  },
};
