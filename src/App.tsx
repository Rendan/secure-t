import { Route, Routes } from "react-router";
import { appRoutes } from "routes/appRoutes";
import { MovieFiltersPage } from "pages/MovieFiltersPage";
import { FC } from "react";
import { BrowserRouter } from "react-router-dom";
import { MoviesPage } from "pages/MoviesPage";

export const App: FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={appRoutes.root()} element={<MovieFiltersPage />} />
        <Route path={appRoutes.movies()} element={<MoviesPage />} />
      </Routes>
    </BrowserRouter>
  );
};
