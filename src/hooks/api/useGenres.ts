import { GenresResponse } from "types/responses/index";
import { useQuery } from "react-query";
import { movieRepository } from "repositories/movieRepository";

export const useGenres = () => {
  const { data, isFetching } = useQuery<GenresResponse>(["genres"], () =>
    movieRepository.genres()
  );

  return {
    genres: data?.genres,
    isFetching,
  };
};
