import { AxiosError } from "axios";
import { useMemo } from "react";
import { useInfiniteQuery } from "react-query";
import { movieRepository } from "repositories/movieRepository";
import { Movie } from "types/domain/entities/Movie";
import { InfiniteResponse } from "types/domain/http";
import { MovieFilters } from "types/domain/ui/Movie";

export const useMovies = (movieFilters: MovieFilters) => {
  const {
    data,
    isFetching,
    isFetchingNextPage,
    hasNextPage,
    isFetched,
    fetchNextPage,
  } = useInfiniteQuery<
    InfiniteResponse<Movie>,
    AxiosError,
    InfiniteResponse<Movie>,
    [string, number | undefined, number | undefined, number[] | undefined]
  >(
    [
      "movies",
      movieFilters?.primaryReleaseYear,
      movieFilters?.voteAverage,
      movieFilters?.genreIds,
    ],
    async ({ queryKey, pageParam = 1 }) => {
      const [_, primaryReleaseYear, voteAverage, genreIds] = queryKey;
      return movieRepository.discoverMovies(pageParam, {
        primaryReleaseYear,
        voteAverage,
        genreIds,
      });
    },
    {
      enabled: !!movieFilters,
      getNextPageParam: (lastPage, allPages) =>
        lastPage.totalPages > allPages.length ? lastPage.page + 1 : null,
    }
  );

  const movies = useMemo(() => {
    if (!data) {
      return [];
    }

    const accumulatedMovies: Movie[] = data.pages.reduce<Movie[]>(
      (acc, page) => {
        return [...acc, ...page.results];
      },
      []
    );

    return accumulatedMovies;
  }, [data?.pages]);

  return {
    movies,
    hasNextPage,
    isFetching,
    isFetchingNextPage,
    isFetched,
    fetchNextPage,
  };
};
