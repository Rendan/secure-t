import { MovieResponse } from "types/responses/index";
import { useQuery } from "react-query";
import { movieRepository } from "repositories/movieRepository";

export const useMovie = (id: number) => {
  const { data, isFetching } = useQuery<MovieResponse>(["movie", id], () =>
    movieRepository.movie(id)
  );

  return {
    movie: data,
    isFetching,
  };
};
