import { Genre } from "types/domain/entities/Genre";
import { Movie, MovieFull } from "types/domain/entities/Movie";
import { InfiniteResponse } from "types/domain/http";

export type DiscoverMoviesResponse = InfiniteResponse<Movie>;
export type GenresResponse = { genres: Genre[] };
export type MovieResponse = MovieFull;
