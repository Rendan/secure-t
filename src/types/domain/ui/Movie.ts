import { Movie } from "types/domain/entities/Movie";

export type MovieFilters = Partial<
  Pick<Movie, "genreIds" | "title" | "voteAverage">
> & { primaryReleaseYear?: number; genreIds?: number[] };
