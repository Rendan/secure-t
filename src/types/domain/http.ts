export type InfiniteResponse<T> = {
  page: number;
  results: Array<T>;
  totalPages: number;
  totalResults: number;
};
