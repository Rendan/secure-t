import {
  Select as DefaultSelect,
  MenuItem,
  SelectChangeEvent,
  InputLabel,
  FormControl,
  Box,
} from "@mui/material";
import { useGenres } from "hooks/api/useGenres";
import { FC, useCallback } from "react";

type SelectProps = {
  value: number[];
  id?: string;
  disabled?: boolean;
  onChange: (genreIds: number[]) => void;
};

export const Select: FC<SelectProps> = (props) => {
  const { value, disabled, id = "genre-select", onChange } = props;
  const { genres, isFetching } = useGenres();

  const handleChange = useCallback(
    (event: SelectChangeEvent<number[]>) => {
      if (typeof event.target.value !== "string") {
        onChange(event.target.value);
      }
    },
    [onChange]
  );

  return (
    <Box sx={{ width: 240 }}>
      <FormControl size="small" fullWidth>
        <InputLabel size="small" id={id}>
          Select multile genres
        </InputLabel>

        <DefaultSelect
          labelId={id}
          multiple
          size="small"
          variant="outlined"
          label="Select multile genres"
          onChange={handleChange}
          disabled={disabled || isFetching}
          value={value}
        >
          {genres?.map((genre) => (
            <MenuItem key={genre.id} value={genre.id}>
              {genre.name}
            </MenuItem>
          ))}
        </DefaultSelect>
      </FormControl>
    </Box>
  );
};
