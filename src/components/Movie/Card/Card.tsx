import { Box, Paper, CardMedia, Grid, Typography, Stack } from "@mui/material";
import { FC, useCallback, useMemo } from "react";
import { Movie } from "types/domain/entities/Movie";

type CardProps = {
  movie: Movie;
  onClick: (movieId: number) => void;
};

export const Card: FC<CardProps> = (props) => {
  const { movie, onClick } = props;

  const handleClick = useCallback(() => {
    onClick(movie.id);
  }, [movie, onClick]);

  const data = useMemo(() => {
    return [
      { title: "Overview", value: movie.overview },
      { title: "Title", value: movie.title },
      { title: "Rating", value: movie.voteAverage },
      { title: "Release Date", value: movie.releaseDate },
    ];
  }, [movie]);

  return (
    <Paper variant="outlined" onClick={handleClick}>
      <Box p={2}>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <CardMedia
              component="img"
              image={`https://image.tmdb.org/t/p/original${movie.posterPath}`}
            />
          </Grid>
          <Grid item xs={9}>
            {data.map((item) => (
              <Grid direction="row" spacing={2} container>
                <Grid item xs={2}>
                  <Typography>
                    <b>{`${item.title}: `}</b>
                  </Typography>
                </Grid>
                <Grid item xs={10}>
                  <Typography>{item.value}</Typography>
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};
