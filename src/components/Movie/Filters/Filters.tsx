import { MovieFilters } from "types/domain/ui/Movie";
import { FC, FormEventHandler, useCallback, useEffect, useState } from "react";
import { Button, Grid, TextField } from "@mui/material";
import { Select } from "components/Genre/Select";

interface FormElements extends HTMLFormControlsCollection {
  title: HTMLInputElement;
  genreIds: HTMLInputElement;
  voteAverage: HTMLInputElement;
  primaryReleaseYear: HTMLInputElement;
}
interface UsernameFormElement extends HTMLFormElement {
  readonly elements: FormElements;
}

type FiltersProps = {
  initialMovieFilters?: MovieFilters;
  disabled?: boolean;
  onSubmit: (filters: MovieFilters) => void;
};

export const Filters: FC<FiltersProps> = (props) => {
  const { initialMovieFilters, disabled, onSubmit } = props;
  const [genreIds, setGenreIds] = useState<number[]>([]);

  useEffect(() => {
    setGenreIds(initialMovieFilters?.genreIds || []);
  }, [initialMovieFilters?.genreIds]);

  const handleSubmit: FormEventHandler<UsernameFormElement> = useCallback(
    (event) => {
      event.preventDefault();

      const { title, primaryReleaseYear, voteAverage } =
        event.currentTarget.elements;

      onSubmit({
        genreIds,
        title: title.value,
        primaryReleaseYear: primaryReleaseYear.value
          ? Number(primaryReleaseYear.value)
          : undefined,
        voteAverage: voteAverage.value ? Number(voteAverage.value) : undefined,
      });
    },
    [onSubmit, genreIds]
  );

  const handleChangeGenres = useCallback(
    (genreIds: number[]) => {
      setGenreIds(genreIds);
    },
    [setGenreIds]
  );

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={4} justifyContent="center" alignItems="center">
        <Grid item>
          <TextField
            fullWidth
            size="small"
            variant="outlined"
            placeholder="Title"
            name="title"
            disabled
          />
        </Grid>
        <Grid item>
          <TextField
            fullWidth
            size="small"
            variant="outlined"
            defaultValue={initialMovieFilters?.primaryReleaseYear}
            key={initialMovieFilters?.primaryReleaseYear}
            type="number"
            placeholder="Year"
            name="primaryReleaseYear"
            disabled={disabled}
          />
        </Grid>
        <Grid item>
          <TextField
            fullWidth
            size="small"
            variant="outlined"
            defaultValue={initialMovieFilters?.voteAverage}
            key={initialMovieFilters?.voteAverage}
            type="number"
            placeholder="Rating"
            name="voteAverage"
            disabled={disabled}
          />
        </Grid>
        <Grid item>
          <Select
            value={genreIds}
            onChange={handleChangeGenres}
            disabled={disabled}
          />
        </Grid>
        <Grid item>
          <Button disabled={disabled} variant="contained" type="submit">
            Search
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};
