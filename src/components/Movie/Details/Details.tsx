import {
  Box,
  CardMedia,
  CircularProgress,
  Container,
  Grid,
  Typography,
} from "@mui/material";
import { useMovie } from "hooks/api/useMovie";
import { FC, useMemo } from "react";

type DetailsProps = {
  movieId: number;
};

export const Details: FC<DetailsProps> = (props) => {
  const { movieId } = props;

  const { movie, isFetching } = useMovie(movieId);

  const data = useMemo(() => {
    if (!movie) {
      return [];
    }

    return [
      { title: "Overview", value: movie.overview },
      { title: "Title", value: movie.title },
      { title: "Rating", value: movie.voteAverage },
      { title: "Release Date", value: movie.releaseDate },
      { title: "Runtime", value: movie.runtime },
      {
        title: "Genres",
        value: movie.genres.map(({ name }) => name).join(", "),
      },
    ];
  }, [movie]);

  if (isFetching) {
    return <CircularProgress />;
  }

  return (
    <Container maxWidth="md">
      <Box ml={-4} mr={-4} mb={4}>
        <CardMedia
          component="img"
          image={`https://image.tmdb.org/t/p/original${movie?.backdropPath}`}
        />
      </Box>
      {data.map((item) => (
        <Grid direction="row" spacing={2} container>
          <Grid item xs={2}>
            <Typography>
              <b>{`${item.title}: `}</b>
            </Typography>
          </Grid>
          <Grid item xs={10}>
            <Typography>{item.value}</Typography>
          </Grid>
        </Grid>
      ))}
    </Container>
  );
};
