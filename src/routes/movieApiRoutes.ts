export const movieApiRoutes = {
  discoverMoviesPath: () => "/discover/movie",
  genreMovieListPath: () => "/genre/movie/list",
  moviePath: (id: number | string) => `/movie/${id}`,
};
