export const appRoutes = {
  root: () => "/",
  movies: () => "/movies",
  movie: (movieId: number | string) => `/movies/${movieId}`,
};
